﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AdTudent.Web.API.App_Start;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using AdTudent.Web.API.Providers;
using AdTudent.Web.API.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Neo4j.AspNet.Identity;
using Neo4jClient;


namespace AdTudent.Web.API
{
    public partial class Startup
    {
        static Startup()
        {

        }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        private void ConfigureNeo4J(IAppBuilder app)
        {
            app.CreatePerOwinContext(() =>
            {
                var gc = new GraphClient(new Uri("http://localhost.:7474/db/data"));
                gc.Connect();
                var gcw = new GraphClientWrapper(gc);
                return gcw;
            });
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            ConfigureNeo4J(app);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };

             //Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);


            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "303115128659-ak2lkshsck4js5rcg16gsi4185df83h6.apps.googleusercontent.com",
                ClientSecret = "Eug2tehABeveefznOLmxjCmb",
                Provider = new GoogleAuthProvider()
            };
            app.UseGoogleAuthentication(googleAuthOptions);

            ////app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            ////{
            ////    ClientId = "303115128659-ak2lkshsck4js5rcg16gsi4185df83h6.apps.googleusercontent.com",
            ////    ClientSecret = "Eug2tehABeveefznOLmxjCmb"
            ////});

            //app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
           

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "303115128659-ak2lkshsck4js5rcg16gsi4185df83h6.apps.googleusercontent.com",
            //    ClientSecret = "Eug2tehABeveefznOLmxjCmb",
            //    Provider = new GoogleAuthProvider()
            //});
            
            
        }
    }
}