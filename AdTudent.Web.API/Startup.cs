﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using AdTudent.DAL.Repositories;
using AdTudent.Web.API.Models;
using AdTudent.Web.API.Providers;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Neo4j.AspNet.Identity;
using Neo4jClient;
using Owin;
using ProfileRepo = AdTudent.DAL.Repositories.ProfileRepo;

[assembly: OwinStartup(typeof(AdTudent.Web.API.Startup))]

namespace AdTudent.Web.API
{
    public partial class Startup
    {
       
        public void Configuration(IAppBuilder app)
        {

            var builder = new ContainerBuilder();

            // STANDARD WEB API SETUP:

            // Get your HttpConfiguration. In OWIN, you'll create one
            // rather than using GlobalConfiguration.
            var config = new HttpConfiguration();

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Run other optional steps, like registering filters,
            // per-controller-type services, etc., then set the dependency resolver
            // to be Autofac.
            builder.Register<IGraphClient>(context =>
            {
                var graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"));
                graphClient.Connect();
                return graphClient;

            }).SingleInstance();

            builder.RegisterType<AdvertisementRepo>().As<IAdvertisementRepo>();
            builder.RegisterType<ProfileRepo>().As<IProfileRepo>();
            builder.RegisterType<ContactRepo>().As<IContactRepo>();
            builder.RegisterType<ManagementRepo>().As<IManagementRepo>();
            builder.RegisterType<HomeRepo>().As<IHomeRepo>();
           

            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            // OWIN WEB API SETUP:

            // Register the Autofac middleware FIRST, then the Autofac Web API middleware,
            // and finally the standard Web API middleware.
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

            ConfigureAuth(app);
        }

    }
}
