﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.UI;
using Neo4jClient.Cypher;

namespace AdTudent.Web.API.Controllers
{
    public class UploadController : ApiController
    {
        private readonly JavaScriptSerializer _js = new JavaScriptSerializer { MaxJsonLength = 41943040 };
        private readonly string _storageRoot = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FileUploadPath"]);
        public bool _isReusable { get { return false; } }

        #region Get
        private HttpResponseMessage DownloadFileContent()
        {
            var filename = HttpContext.Current.Request["f"];
            var filePath = _storageRoot+"\\" + filename;
            if (File.Exists(filePath))
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(new FileStream(filePath, FileMode.Open, FileAccess.Read));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = filename
                };
                return response;
            }
            return ControllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "");
        }

        private HttpResponseMessage DownloadFileList()
        {
            var files =
            new DirectoryInfo(_storageRoot)
                .GetFiles("*", SearchOption.TopDirectoryOnly)
                .Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden))
                .Select(f => new FilesStatus(f))
                .ToArray();
            HttpContext.Current.Response.AppendHeader("Content-Disposition", "inline; filename=\"files.json\"");
            return ControllerContext.Request.CreateResponse(HttpStatusCode.OK, _js.Serialize(files));
        }

        [HttpGet]
        [ActionName("GetFile")]
        public HttpResponseMessage GetFile()
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["f"]))
            {
                return DownloadFileContent();
            }
            return DownloadFileList();
        }

        //[HttpGet]
        //[ActionName("GetThumbnail")]
        //public HttpResponseMessage GetThumbnail()
        //{
            
        //    var fileName = HttpContext.Current.Request["f"];
        //    if (string.IsNullOrEmpty(fileName))
        //        return null;

        //    byte[] ThumbNailbytes;
        //    var fullPath = _storageRoot + fileName;
        //    try
        //    {
        //        using (Image image = Image.FromFile(fullPath))
        //        {
        //            var ratioX = (double) 80/image.Width;
        //            var ratioY = (double) 80/image.Height;
        //            var ratio = Math.Min(ratioX, ratioY);
        //            var newWidth = (int) (image.Width*ratio);
        //            var newHeight = (int) (image.Height*ratio);
        //            var newImage = new Bitmap(newWidth, newHeight);
        //            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
        //            ImageConverter converter = new ImageConverter();
        //            ThumbNailbytes = (byte[]) converter.ConvertTo(newImage, typeof (byte[]));
        //            newImage.Dispose();

        //            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
        //            response.Content = new StreamContent(new MemoryStream(ThumbNailbytes));
        //            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //            {
        //                FileName = fileName
        //            };
        //            return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
                
        //    }
        //    return ControllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "");
        //}
        #endregion

        #region Post & Put
        public HttpResponseMessage Post()
        {
            return UploadFile(HttpContext.Current);
        }

        public HttpResponseMessage Put()
        {
            var returnValue =  UploadFile(HttpContext.Current);
            return returnValue;
        }

        private HttpResponseMessage UploadFile(HttpContext context)
        {
            var statuses = new List<FilesStatus>();
            var headers = context.Request.Headers;

            if (string.IsNullOrEmpty(headers["X-File-Name"]))
            {
                UploadWholeFile(context, statuses);
            }
            else
            {
                UploadPartialFile(headers["X-File-Name"], context, statuses);
            }

            return WriteJsonIframeSafe(context, statuses);
        }

        private HttpResponseMessage WriteJsonIframeSafe(HttpContext context, List<FilesStatus> statuses)
        {
            string httpaccept = null;
            try
            {
                httpaccept = context.Request["HTTP_ACCEPT"];
            }
            catch (Exception ex)
            {
                
            }
            

            context.Response.AddHeader("Vary", "Accept");
            var response = new HttpResponseMessage()
            {
                Content = new StringContent(_js.Serialize(statuses.ToArray()))
            };
            if (httpaccept == string.Empty && httpaccept.Contains("application/json"))
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
            else
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            }
            return response;
        }

        // Upload partial file
        private void UploadPartialFile(string fileName, HttpContext context, List<FilesStatus> statuses)
        {
            if (context.Request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
            var inputStream = context.Request.Files[0].InputStream;
            var fullName = _storageRoot + Path.GetFileName(fileName);

            using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
            {
                var buffer = new byte[1024];

                var l = inputStream.Read(buffer, 0, 1024);
                while (l > 0)
                {
                    fs.Write(buffer, 0, l);
                    l = inputStream.Read(buffer, 0, 1024);
                }
                fs.Flush();
                fs.Close();
            }
            statuses.Add(new FilesStatus(new FileInfo(fullName)));
        }

        // Upload entire file
        private void UploadWholeFile(HttpContext context, List<FilesStatus> statuses)
        {
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                
                var file = context.Request.Files[i];
                
                //var extention = Path.GetExtension(file.FileName);
                var extention = GetFileExtensionFromData(file);
                

                //string fullPath = _storageRoot + Path.GetFileNameWithoutExtension(file.FileName) +
                //                  "$" + long.Parse(DateTime.Now.ToString("yyyyMMddHHmmss")) +
                //                  extention;

                string fullPath = _storageRoot  +  long.Parse(DateTime.Now.ToString("yyyyMMddHHmmss")) +
                                  extention;
                    
                Directory.CreateDirectory(_storageRoot);
                file.SaveAs(fullPath);
                string fullName = Path.GetFileName(Path.GetFileName(fullPath));
                statuses.Add(new FilesStatus(fullName, file.ContentLength, fullPath));
            }
        }

        private string GetFileExtensionFromData(HttpPostedFile file)
        {
            return  FileIdentifier.GetImageFileType(file);




        }
        #endregion

        #region Delete

        public HttpResponseMessage Delete()
        {
            var filePath = _storageRoot + HttpContext.Current.Request["f"];
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            return ControllerContext.Request.CreateResponse(HttpStatusCode.OK, "");
        }
        #endregion
    }

    #region FileStatus
    public class FilesStatus
    {
        public const string HandlerPath = "/";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }

        public FilesStatus()
        {
        }

        public FilesStatus(FileInfo fileInfo)
        {
            SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName);
        }

        public FilesStatus(string fileName, int fileLength, string fullPath)
        {
            SetValues(fileName, fileLength, fullPath);
        }

        private void SetValues(string fileName, int fileLength, string fullPath)
        {
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "api/Upload?f=" + fileName;
            delete_url = HandlerPath + "api/Upload?f=" + fileName;
            delete_type = "DELETE";
            var ext = Path.GetExtension(fullPath);
            var fileSize = ConvertBytesToMegabytes(new FileInfo(fullPath).Length);
            if (fileSize > 3 || !IsImage(ext))
            {
                thumbnail_url = "/Content/img/generalFile.png";
            }
            else
            {
                thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
            }
        }

        private bool IsImage(string ext)
        {
            return ext == ".gif" || ext == ".jpg" || ext == ".png" || ext == ".jpeg";
        }

        private string EncodeFile(string fileName)
        {
            byte[] bytes;
            using (Image image = Image.FromFile(fileName))
            {
                var ratioX = (double)80 / image.Width;
                var ratioY = (double)80 / image.Height;
                var ratio = Math.Min(ratioX, ratioY);
                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);
                var newImage = new Bitmap(newWidth, newHeight);
                Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
                ImageConverter converter = new ImageConverter();
                bytes = (byte[])converter.ConvertTo(newImage, typeof(byte[]));
                newImage.Dispose();
            }
            return Convert.ToBase64String(bytes);
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }
    #endregion

    public static class HttpPostedFileBaseExtensions
    {
        public static Byte[] ToByteArray(this HttpPostedFile value)
        {
            if (value == null)
                return null;
            var array = new Byte[value.ContentLength];
            value.InputStream.Position = 0;
            value.InputStream.Read(array, 0, value.ContentLength);
            return array;
        }
    }

    public class FileIdentifier
    {

        private static bool Is_gif(byte[] bte)
        {
            //GIF : 47-49-46-38-39-61   ---- 71- 73-70 - 56- 57- 97
            //GIF : 47-49-46-38-37-61   ---- 71- 73-70 - 56- 55- 97
            if (bte[0] == 71 && bte[1] == 73 && bte[2] == 70 &&
                bte[3] == 56 && (bte[4] == 57 || bte[4] == 55) && bte[5] == 97)
                return true;
            
            return false;
        }

        private static bool Is_jpg(byte[] bte)
        {
            //FF D8 FF E0
            //255 216 255 224
            if (bte[0] == 255 && bte[1] == 216 && bte[2] == 255 && bte[3] == 224)
                return true;
            
            return false;
        }
        private static bool Is_mp3(byte[] bte)
        {
            //FF FB or 49 44 33
            //255 251 or 73 68 51
            if (bte[0] == 255 && bte[1] == 251)
                return true;
            if (bte[0] == 73 && bte[1] == 68 && bte[2] == 51)
                return true;

            return false;
        }
        private static bool Is_pdf(byte[] bte)
        {
            //Hex = 25 50 44 46
            //V = 37 80 68 70

            if (bte[0] == 37 && bte[1] == 80 && bte[2] == 68 && bte[3] == 70)
                return true;
            return false;
        }

        private static bool Is_png(byte[] bte)
        {
            //89 50 4E 47 0D 0A 1A 0A

            if (bte[0] == 137 && bte[1] == 80 && bte[2] == 78 && bte[3] == 71&&
             bte[4] == 13 && bte[5] == 10 && bte[6] == 26&& bte[7] == 10)
                return true;
            return false;
        }
        private static bool Is_mp4(byte[] bte)
        {
            //HEX = 00 00 00 nn 66 74 79 70 --> V= 0   0  0 24 102 116 121 112 
            //HEX = 33 67 70 35 --> v= 51 103 112 53

            if (bte[0] == 0 && bte[1] == 0 && bte[2] == 0 &&
                /*bte[3] == nn*/
                bte[4] == 102 && bte[5] == 116 && bte[6] == 121 && bte[7] == 112)
                return true;
            if (bte[0] == 51 && bte[1] == 103 && bte[2] == 112 && bte[2] == 53)
                return true;

            return false;
        }
        public static String GetImageFileType(HttpPostedFile file)
        {
            //var bte = file.InputStream.
            var bte = new byte[20];
            using (var ms = new MemoryStream(file.ToByteArray()))
            {
                for (int i = 0; i < 20; i++)
                {
                    bte[i] = (byte)ms.ReadByte();
                }

                if (Is_gif(bte))
                    return ".gif";

                if (Is_jpg(bte))
                    return ".jpg";

                if (Is_mp3(bte))
                    return ".mp3";

                if (Is_mp4(bte))
                    return ".mp4";

                if (Is_pdf(bte))
                    return ".pdf";

                if (Is_png(bte))
                    return ".png";
                
                return ".unknown";
            }
        }

       
    }
}
