﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using AdTudent.Web.API.App_Start;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Neo4j.AspNet.Identity;
using Newtonsoft.Json.Linq;

namespace AdTudent.Web.API.Controllers
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Role")]
    public class RoleController : ApiController
    {
        public ApplicationUserManager _userManager { get; set; }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        [Route("GetRolesByEmail")]
        public IHttpActionResult GetRolesByEmail(JObject data)
        {
            dynamic json = data;

            string email = json["email"];

            var user = UserManager.FindByEmail(email);

            return Ok(user.Roles);
        }

        [HttpPost]
        [Route("GetRolesById")]
        public IHttpActionResult GetRolesById(JObject data)
        {
            dynamic json = data;
            string username = json["username"];

            var user = UserManager.FindByName(username);

            return Ok(user.Roles);
        }

        [HttpPost]
        [Route("AddUserToAdmin")]
        public async Task<IHttpActionResult> AddUserToAdmin(JObject data)
        {
            dynamic json = data;
            string username = json["username"];

            ApplicationUser user = await UserManager.FindByNameAsync(username);

            if (user.Roles.Contains("Admin"))
            {
                return BadRequest("User has already admin access");
            }

            await UserManager.AddToRoleAsync(user.Id, "Admin");

            return Ok();
        }

        [HttpPost]
        [Route("RemoveUserFromAdmin")]
        public async Task<IHttpActionResult> RemoveUserFromAdmin(JObject data)
        {
            dynamic json = data;
            string username = json["username"];

            ApplicationUser user = await UserManager.FindByNameAsync(username);
       
            if (!user.Roles.Contains("Admin"))
            {
                return BadRequest("User has no admin role ");
            }
            await UserManager.RemoveFromRoleAsync(user.Id.ToString(), "Admin");
           
            return Ok();
        }

        [HttpGet]
        [Route("Block")]
        public OkNegotiatedContentResult<ApplicationUser> BlockUser()
        {
            var user = UserManager.FindByName("");

            var result = UserManager.SetLockoutEnabled(user.Id, true);
            var query = UserManager.GetLockoutEnabled(user.Id);

            if (result.Succeeded)
            {
                // result = await UserManager.SetLockoutEndDateAsync(user.Id, DateTimeOffset.MaxValue);
            }
            return Ok(user);
        }

    }
}
