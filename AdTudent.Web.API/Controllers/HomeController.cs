﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AdTudent.DAL.Repositories;
using AdTudent.Domain.POCO;
using AdTudent.Web.API.Models;
using Microsoft.AspNet.Identity;
using Neo4j.AspNet.Identity;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json.Linq;

namespace AdTudent.Web.API.Controllers
{
    [Authorize(Roles = "User")]
    [RoutePrefix("api/Home")]
    public class HomeController : ApiController
    {
        public IHomeRepo _homeRepo { get; set; }
        public string CurrentUsername { get; set; }


        public HomeController(IHomeRepo repo)
        {
            _homeRepo = repo;
            CurrentUsername = User.Identity.GetUserName();
        }


        [HttpGet]
        [Route("GetList")]
        public IHttpActionResult GetList()
        {
            // Show the Latest Advertisments to User
            var result = _homeRepo.HomeUpdate(CurrentUsername);

            return Ok(result);
        }

        [HttpPost]
        [Route("Search")]
        public IHttpActionResult Search(JObject data)
        {
            dynamic json = data;

            string searchText = json["text"].ToString();

            var result = _homeRepo.Search(searchText);

            return Ok(result);
        }
    }
}

