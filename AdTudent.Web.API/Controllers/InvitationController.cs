﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using AdTudent.DAL.Util;
using AdTudent.Web.API.Models;
using Microsoft.AspNet.Identity;
using Neo4j.AspNet.Identity;
using Newtonsoft.Json.Linq;

namespace AdTudent.Web.API.Controllers
{
    [Authorize(Roles = "User")]
    [RoutePrefix("api/Invitation")]
    public class InvitationController : ApiController
    {

        [Route("SendInvitation")]
        public async Task<IHttpActionResult> SendInvitation(JObject data)
        {
            // Get the Id of Current User
            string id = User.Identity.GetUserId();
            string email = User.Identity.GetUserName();

            dynamic json = data;
            string destination = json["Email"].ToString();
            if (!RegexUtilities.IsValidEmail(destination))
            {
                return InternalServerError(new Exception("متن وارد شده آدرس ایمیل نمی باشد"));
            }

            // Set Url Callback
            var callBack = Url.Route("DefaultApi", new { controller = "Account/Register", sourceId = id , targetEmail= destination});

            var body =
                string.Format("<p style='direction:rtl;'>از طرف ایمیل  {0} </p><p style='direction:rtl'>برای ثبت نام در سایت <a href=\"" + callBack +
                              "\">کلیک</a> کنید </p>", email);
            
            var message = new MailMessage();
            message.To.Add(new MailAddress(destination));
            message.From = new MailAddress("AdTudent@hotmail.com");
            message.Subject = "Invitation";
            message.Body = body;
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "AdTudent@hotmail.com",
                    Password = "shbu@project"
                };

                smtp.Credentials = credential;
                smtp.Host = "smtp-mail.outlook.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }

            return Ok();
        }

        
    }
}
