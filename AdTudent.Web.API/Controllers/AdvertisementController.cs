﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Http;
using AdTudent.DAL.Repositories;
using AdTudent.Domain.POCO;
using AdTudent.Web.API.Models;
using Microsoft.AspNet.Identity;
using Neo4j.AspNet.Identity;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json.Linq;
using SnowMaker;

namespace AdTudent.Web.API.Controllers
{
    [Authorize(Roles = "User")]
    [RoutePrefix("api/Advertisement")]
    public class AdvertisementController : ApiController
    {
        private readonly string _storageRoot = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FileUploadPath"]);
        public string CurrentUsername { get; set; }
        public IAdvertisementRepo _repoAdvertisement { get; set; }

        public AdvertisementController(IAdvertisementRepo repoAdvertisement)
        {
            // Get The Id of Current User
            CurrentUsername = User.Identity.GetUserName();
            _repoAdvertisement = repoAdvertisement;
        }

        [HttpPost]
        [Route("CreateAd")]
        public IHttpActionResult CreateAd(Advertisement advertisement)
        {
            // Unique Id Generator with SnowMaker
            var path = HostingEnvironment.MapPath("~/App_Data/UniqueId");
            SnowMaker.UniqueIdGenerator IdG = new SnowMaker.UniqueIdGenerator(new DebugOnlyFileDataStore(path));

            // Generate Next Id for Advertisement
            advertisement.Id = IdG.NextId("Advertisement");
            // Set Publish DateTime
            advertisement.PublishDateTime = DateTimeOffset.Now;

            // Create New Advertisement for Current User
            _repoAdvertisement.CreateAd(advertisement, CurrentUsername);

            return Ok();
        }

        [HttpGet]
        [Route("RetriveAd")]
        public IHttpActionResult RetrieveAd()
        {
            // Retrive all Advetisement of Current User
            var result = _repoAdvertisement.RetriveAds(CurrentUsername);
            return Ok(result);
        }

        [HttpPost]
        [Route("UpdateAd")]
        public IHttpActionResult UpdateAd(Advertisement advertisement)
        {

            // Update the Current Advertisement of User
            _repoAdvertisement.UpdateAd(advertisement, CurrentUsername);

            return Ok();
        }

        [HttpPost]
        [Route("DeleteAd")]
        public IHttpActionResult DeleteAd(JObject node)
        {

            // Assign Id of Current Advertisement
            dynamic adId = (long)node["id"];

            // Delete Current Advertisement
          _repoAdvertisement.DeleteAd(adId, CurrentUsername);

            return Ok();
        }


        [HttpPost]
        [Route("SetImage")]
        public IHttpActionResult SetImage(JObject data)
        {
            dynamic json = data;
            var adId = (long)data["Id"];
            var ImageUrl = AdvertisementRepo.SaveEncodedImage("Image", json.Image.ToString(), CurrentUsername, _storageRoot);

            var item = new Advertisement()
            {
                ImageUrl = ImageUrl,
                Id = adId
            };

           _repoAdvertisement.SetImage(item, CurrentUsername);
            return Ok();
        }

        [HttpGet]
        [Route("GetImage")]
        public IHttpActionResult GetImage(JObject data)
        {
            dynamic json = data;
            long adId = (long)json.Id;

            var advertisement = _repoAdvertisement.LoadById(adId, CurrentUsername);
            if (advertisement != null && advertisement.ImageUrl != null)
            {
                var filename = advertisement.ImageUrl;
                var filePath = _storageRoot + CurrentUsername + "\\" + advertisement.ImageUrl;
                if (File.Exists(filePath))
                {
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StreamContent(new FileStream(filePath, FileMode.Open, FileAccess.Read));
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = filename
                    };
                    return Ok(response);
                }
            }
            return Ok(ControllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, ""));

        }
    }
}
