﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using AdTudent.DAL.Repositories;
using AdTudent.Domain.POCO;
using AdTudent.Web.API.Models;
using Microsoft.AspNet.Identity;
using Neo4j.AspNet.Identity;
using Neo4jClient;
using Newtonsoft.Json.Linq;

namespace AdTudent.Web.API.Controllers
{
    [Authorize(Roles = "User")]
    [RoutePrefix("api/Profile")]
    public class ProfileController : ApiController
    {
        private readonly string _storageRoot = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ProfileUploadPath"]);
        public string CurrentUsername { get; set; }

        public IProfileRepo _profileRepo { get; set; }

        public ProfileController(IProfileRepo profileRepo)
        {
            CurrentUsername = User.Identity.GetUserName();
            _profileRepo = profileRepo;
        }

        [HttpPost]
        [Route("CreateProfile")]
        public IHttpActionResult CreateProfile(JObject data)
        {
            dynamic json = data;

            var profile = new Profile()
            {
                DisplayName = json.DisplayName,
                FirstName = json.FirstName,
                LastName = json.LastName,
                AvatarUrl = json.AvatarUrl
            };

            _profileRepo.CreatProfile(profile, CurrentUsername);
            return Ok();
        }

        [HttpPost]
        [Route("UpdateProfile")]
        public IHttpActionResult UpdateProfile(Profile profile)
        {
            _profileRepo.UpdateProfile(profile, CurrentUsername);
            return Ok();
        }


        [HttpPost]
        [Route("SetAvatar")]
        public IHttpActionResult SetAvatar(JObject data)
        {
            dynamic json = data;

            var avatarUrl = SaveEncodedImage("avatar", json.profile.ToString(), CurrentUsername);

            var item = new Profile()
            {

                AvatarUrl = avatarUrl,

            };

            var profile = _profileRepo.UpdateAvatar(item, CurrentUsername);

            return Ok(profile);
        }

        private string SaveEncodedImage(string fileName, string encodedData, string username)
        {
            var extentionPart = encodedData.Substring(0, encodedData.IndexOf(';'));
            var extension = extentionPart.Substring(encodedData.IndexOf('/') + 1, extentionPart.Length - encodedData.IndexOf('/') - 1);
            var data = encodedData.Substring(encodedData.IndexOf(',') + 1, encodedData.Length - encodedData.IndexOf(',') - 1);
            var bytes = Convert.FromBase64String(data);
            var filePath = _storageRoot + username + "\\" + fileName + "." + extension;
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return fileName + "." + extension;
        }

        [Authorize]
        [ActionName("GetAvatar")]
        [HttpGet]
        public HttpResponseMessage GetAvatar()
        {
            var username = RequestContext.Principal.Identity.GetUserName();
            var profile = _profileRepo.LoadProfile(username);
            if (profile != null && profile.AvatarUrl != null)
            {
                var filename = profile.AvatarUrl;
                var filePath = _storageRoot + username + "\\" + profile.AvatarUrl;
                if (File.Exists(filePath))
                {
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StreamContent(new FileStream(filePath, FileMode.Open, FileAccess.Read));
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = filename
                    };
                    return response;
                }
            }
            return ControllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "");
        }
    }
}
