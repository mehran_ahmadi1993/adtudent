﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AdTudent.DAL.Repositories;
using AdTudent.Domain.POCO;
using AdTudent.Web.API.Models;
using Neo4j.AspNet.Identity;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json.Linq;

namespace AdTudent.Web.API.Controllers
{

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Management")]
    public class ManagementController : ApiController
    {
        public IManagementRepo _managementRepo { get; set; }

        public ManagementController(IManagementRepo repo)
        {
            _managementRepo = repo;
        }

        [HttpGet]
        [Route("GetLatestAds")]
        public IHttpActionResult GetLatestAds()
        {

            // Fetch the List of 10 Latest Advertisements from Database base on published DateTime
            var result = _managementRepo.GetLatestAds();

            return Ok(result);
        }

        [HttpGet]
        [Route("GetAds")]
        public IHttpActionResult GetAds()
        {

            // Fetch the List of all Advertisements from Database
            var result = _managementRepo.GetAds();

            return Ok(result);
        }

        [HttpPost]
        [Route("SearchAds")]
        public IHttpActionResult SearchAds(JObject data)
        {
            dynamic json = data;

            string text = json["text"];

            // Search Advertisement base on Title, Adress and Description through searchText
            var result = _managementRepo.SearchAdvertisements(text);

            return Ok(result);
        }

        [HttpGet]
        [Route("GetUsers")]
        public IHttpActionResult GetUsers()
        {
            // Fetch All of Users from Database
            var result = _managementRepo.GetUsers();

            return Ok(result);
        }

        [HttpPost]
        [Route("SearchUser")]
        public IHttpActionResult SearchUser(JObject data)
        {
            dynamic json = data;

            string text = json["text"];

            // Search User in Database through SearchText
            var result = _managementRepo.SearchUser(text);

            return Ok(result);
        }

        [HttpPost]
        [Route("DeleteAd")]
        public IHttpActionResult DeleteAd(JObject data)
        {
            dynamic json = data;

            long text = json["id"];

            // Search User in Database through SearchText
             _managementRepo.DeleteAd(text);

            return Ok();
        }
    }
}
