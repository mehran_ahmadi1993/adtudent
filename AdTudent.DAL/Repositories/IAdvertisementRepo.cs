﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;

namespace AdTudent.DAL.Repositories
{
    public interface IAdvertisementRepo
    {
        void CreateAd(Advertisement advertisement, string username);
        List<Advertisement> RetriveAds(string username);
        Advertisement LoadById(long adId, string username);
        void UpdateAd(Advertisement advertisement, string username);
        void DeleteAd(long id, string username);
        void SetImage(Advertisement advertisement, string username);
    }
}
