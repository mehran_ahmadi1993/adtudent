﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;
using Neo4j.AspNet.Identity;
using Neo4jClient;

namespace AdTudent.DAL.Repositories
{
    public class ManagementRepo:IManagementRepo
    {
        public IGraphClient _graphClient { get; set; }

        public ManagementRepo(IGraphClient gc)
        {
            _graphClient = gc;
        }
        public IList<Advertisement> GetLatestAds()
        {
            var result = _graphClient.Cypher
               .Match("(ad:Advertisement)")
               .Return(ad => ad.As<Advertisement>())
               .OrderByDescending("ad.PublishDateTime")
               .Limit(10)
               .Results.ToList();

            return result;
        }

        public IList<Advertisement> GetAds()
        {
            var result = _graphClient.Cypher
              .Match("(ad:Advertisement)")
              .Return(ad => ad.As<Advertisement>())
              .OrderByDescending("ad.PublishDateTime")
              .Results.ToList();

            return result;
        }

        public IList<ApplicationUser> GetUsers()
        {
            var result = _graphClient.Cypher
               .Match("(u:User)")
               .Return(u => u.As<ApplicationUser>())
               .Results.ToList();

            return result;
        }

        public IList<ApplicationUser> SearchUser(string text)
        {
            var result = _graphClient.Cypher
               .Match("(u:User)")
               .Where("u.UserName =~ {terms}")
               .OrWhere("u.DisplayName =~ {terms}")
               .WithParam("terms", "(?ui).*" + text + ".*")
               .Return<ApplicationUser>("u").Results.ToList();

            return result;
        }

        public IList<Advertisement> SearchAdvertisements(string text)
        {
            var result = _graphClient.Cypher
                .Match("(ad:Advertisement)")
                .Where("ad.Title =~ {terms}")
                .OrWhere("ad.Adress =~ {terms}")
                .OrWhere("ad.Desc =~ {terms}")
                .WithParam("terms", "(?ui).*" + text + ".*")
                .Return<Advertisement>("ad").Results.ToList();

            return result;
        }

        public void DeleteAd(long id)
        {
            _graphClient.Cypher
                .Match("()-[r]->(ad:Advertisement)")
                .Where<Advertisement>(ad => ad.Id == id)
                .Delete("r,ad")
                .ExecuteWithoutResults();
        }
    }
}
