﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdTudent.DAL.Repositories
{
    public class Repository:IRepository
    {
        public Repository(IAdvertisementRepo advertisementRepository, IProfileRepo profileRepository)
        {
            Profiles = profileRepository;
            Advertisements = advertisementRepository;
        }
        public IProfileRepo Profiles { get; set; }
        public IAdvertisementRepo Advertisements { get; set; }
    }
}
