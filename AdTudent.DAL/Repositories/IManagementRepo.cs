﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;
using Neo4j.AspNet.Identity;

namespace AdTudent.DAL.Repositories
{
    public interface IManagementRepo
    {
        IList<Advertisement> GetLatestAds();
        IList<Advertisement> GetAds();
        IList<ApplicationUser> GetUsers();
        IList<ApplicationUser> SearchUser(string text);
        IList<Advertisement> SearchAdvertisements(string text);
        void DeleteAd(long id);
    }
}
