﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;
using AdTudent.Domain.ViewModels;

namespace AdTudent.DAL.Repositories
{
    public interface IHomeRepo
    {
        HomeViewModel HomeUpdate(string username);
        IList<Advertisement> Search(string text);

    }
}
