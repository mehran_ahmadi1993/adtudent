﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Neo4j.AspNet.Identity;
using Neo4jClient;

namespace AdTudent.DAL.Repositories
{
    public class ContactRepo:IContactRepo
    {
        public IGraphClient _graphClient { get; set; }

        public ContactRepo(IGraphClient gc)
        {
            _graphClient = gc;
        }

        public void SetContact(string sourceId, string targetId)
        {
            _graphClient.Cypher
                .Match("(user1:User),(user2:User)")
                .Where<ApplicationUser>(user1 => user1.Id == sourceId)
                .AndWhere<ApplicationUser>(user2 => user2.Id == targetId)
                .Create("user1-[:IS_Friend]->user2")
                .ExecuteWithoutResults();
        }
    }
}
