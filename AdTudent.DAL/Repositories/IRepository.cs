﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdTudent.DAL.Repositories
{
    public interface IRepository
    {
        IProfileRepo Profiles { get; set; }
        IAdvertisementRepo Advertisements { get; set; }
    }
}
