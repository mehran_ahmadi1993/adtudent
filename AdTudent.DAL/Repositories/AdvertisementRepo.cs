﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AdTudent.DAL.Util;
using AdTudent.Domain.POCO;
using Neo4j.AspNet.Identity;
using Neo4jClient;

namespace AdTudent.DAL.Repositories
{
    public class AdvertisementRepo : IAdvertisementRepo
    {
        public IGraphClient _graphClient { get; set; }

        public AdvertisementRepo(IGraphClient gc)
        {
            _graphClient = gc;
        }
        public void CreateAd(Advertisement advertisement, string username)
        {
            _graphClient.Cypher
                .Match("(u:User)")
                .Where<ApplicationUser>(u => u.UserName == username)
                .Create("(u-[:HAS_Published]->(ad:Advertisement {advertisement}))")
                .WithParam("advertisement", advertisement)
                .ExecuteWithoutResults();
        }

        public List<Advertisement> RetriveAds(string username)
        {
            var result = _graphClient.Cypher
                .Match("(u:User)-[:HAS_Published]->(ad:Advertisement)")
                .Where<IdentityUser>(u => u.UserName == username)
                .Return(ad => ad.As<Advertisement>())
                .Results.ToList();

            return result;
        }

        public Advertisement LoadById(long adId, string username)
        {
            var query = "(u:User)-[:HAS_Published]->(ad:Advertisement)";

            var result = _graphClient.Cypher
                  .Match(query)
                  .Where<Advertisement>(ad => ad.Id == adId)
                  .AndWhere<ApplicationUser>(u => u.UserName == username)
                  .Return(ad => ad.As<Advertisement>())
                  .Results.Single();

            return result;
        }

        public void UpdateAd(Advertisement advertisement, string username)
        {
            var adTarget = LoadById(advertisement.Id, username);

            if (adTarget != null)
            {
                Reflection.CopyPropertiesTo(advertisement, adTarget);
            }
            _graphClient.Cypher
                .Match("(u:User)-[:HAS_Published]->(ad:Advertisement)")
                .Where<Advertisement>(ad => ad.Id == advertisement.Id)
                .AndWhere<ApplicationUser>(u => u.UserName == username)
                .Set("ad = {updateAd}")
                .WithParam("updateAd", adTarget)
                .ExecuteWithoutResults();
        }

        public void DeleteAd(long id, string username)
        {
            var query = "(u:User)-[r:HAS_Published]->(ad:Advertisement)";

            _graphClient.Cypher
              .Match(query)
              .Where<Advertisement>(ad => ad.Id == id)
              .AndWhere<ApplicationUser>(u => u.UserName == username)
              .Delete("r , ad")
              .ExecuteWithoutResults();
        }

        public void SetImage(Advertisement advertisement, string username)
        {
            var adTarget = LoadById(advertisement.Id, username);

            if (adTarget != null)
            {
                ReflectionUtil.CopyValues(adTarget, advertisement);
            }

            _graphClient.Cypher
                .Match("(u:User)-[:HAS_Published]->(ad:Advertisement)")
                .Where<Advertisement>(ad => ad.Id == advertisement.Id)
                .AndWhere<ApplicationUser>(u => u.UserName == username)
                .Set("ad = {updateAd}")
                .WithParam("updateAd", adTarget)
                .ExecuteWithoutResults();
        }

        public static string SaveEncodedImage(string fileName, string encodedData, string username, string _storageRoot)
        {
            var extentionPart = encodedData.Substring(0, encodedData.IndexOf(';'));
            var extension = extentionPart.Substring(encodedData.IndexOf('/') + 1, extentionPart.Length - encodedData.IndexOf('/') - 1);
            var data = encodedData.Substring(encodedData.IndexOf(',') + 1, encodedData.Length - encodedData.IndexOf(',') - 1);
            var bytes = Convert.FromBase64String(data);
            var filePath = _storageRoot + username + "\\" + fileName + "." + extension;
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return fileName + "." + extension;
        }
    }
}
