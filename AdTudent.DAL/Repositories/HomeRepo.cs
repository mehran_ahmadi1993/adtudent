﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;
using AdTudent.Domain.ViewModels;
using Neo4j.AspNet.Identity;
using Neo4jClient;

namespace AdTudent.DAL.Repositories
{
    public class HomeRepo : IHomeRepo
    {
        private IGraphClient _graphClient;

        public HomeRepo(IGraphClient gc)
        {
            _graphClient = gc;
        }
        public HomeViewModel HomeUpdate(string username)
        {

            HomeViewModel s = new HomeViewModel();
            // Retrieve Top 25 Advertisements with Users that Published Based On Date the Published
            var friendResult = _graphClient.Cypher
                .Match("(u:User)-[:IS_Friend]->(u2:User)-[:HAS_Published]->(ad:Advertisement)")
                .Where<ApplicationUser>(u => u.UserName == username)
                .ReturnDistinct((ad, u2) => new
                {
                    Username = u2.As<ApplicationUser>(),
                    Advertisement = ad.As<Advertisement>()
                })
                .OrderByDescending("ad.PublishDateTime")
                .Limit(25)
                .Results.ToList();

            if (!friendResult.Any())
            {
                var result = _graphClient.Cypher
                .Match("(u:User)-[:HAS_Published]->(ad:Advertisement)")
                .Where<ApplicationUser>(u => u.UserName == username)
               .ReturnDistinct((u, ad) => new
               {
                   Users = u.As<ApplicationUser>(),
                   Advertisement = ad.As<Advertisement>()
               })
               .OrderByDescending("ad.PublishDateTime")
               .Limit(25)
               .Results.ToList();


                s.Users = result.Select(x => x.Users).ToList();
                s.Advertisements = result.Select(x => x.Advertisement).ToList();


                return s;
            }
            else
            {
                s.Users = friendResult.Select(x => x.Username).ToList();
                s.Advertisements = friendResult.Select(x => x.Advertisement).ToList();
                return s;
            }
        }

        public IList<Advertisement> Search(string text)
        {
            var result = _graphClient.Cypher
                .Match("(ad:Advertisement)")
                .Where("ad.Title =~ {terms}")
                .OrWhere("ad.Adress =~ {terms}")
                .OrWhere("ad.Desc =~ {terms}")
                .WithParam("terms", "(?ui).*" + text + ".*")
                .Return(ad => ad.As<Advertisement>())
                .Results.ToList();

            return result;
        }
    }
}
