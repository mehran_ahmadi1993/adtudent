﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;

namespace AdTudent.DAL.Repositories
{
    public interface IProfileRepo
    {
        Profile CreatProfile(Profile profile, string username);
        Profile UpdateProfile(Profile srcProfile, string username);
        Profile LoadProfile(string username);
        Profile UpdateAvatar(Profile srcProfile, string username);
    }
}
