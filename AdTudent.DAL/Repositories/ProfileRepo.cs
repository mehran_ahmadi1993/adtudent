﻿using System;
using System.Linq;
using AdTudent.DAL.Util;
using AdTudent.Domain.POCO;
using Neo4j.AspNet.Identity;
using Neo4jClient;

namespace AdTudent.DAL.Repositories
{
    public class ProfileRepo:IProfileRepo
    {
     
        public IGraphClient _graphClient { get; set; }

        public ProfileRepo(IGraphClient gc)
        {
            _graphClient = gc;
        }

        public Profile CreatProfile(Profile profile, string username)
        {
            var query = "u-[:HAS_Profile]->(prf:Profile {prf})";

            _graphClient.Cypher
               .Match("(u:User)")
               .Where<ApplicationUser>(u => u.UserName == (username))
               .Create(query)
               .WithParam("prf", profile)
               .ExecuteWithoutResults();

            return profile;
        }

        public Profile UpdateProfile(Profile srcProfile, string username)
        {
            var targetPrf = LoadProfile(username);
            if (targetPrf == null)
            {
                CreatProfile(srcProfile, username);
            }
            else
            {
                Reflection.CopyPropertiesTo( srcProfile, targetPrf);
            }

            var query = "(u:User)-[:HAS_Profile]->(prf:Profile)";

            _graphClient.Cypher
                .Match(query)
                .Where<ApplicationUser>(u => u.UserName == username)
                .Set("prf = {profile}")
                .WithParam("profile", targetPrf)
                .ExecuteWithoutResults();
            return targetPrf;
        }

        public Profile UpdateAvatar(Profile srcProfile, string username)
        {
            var targetPrf = LoadProfile(username);
            if (targetPrf == null)
            {
                CreatProfile(srcProfile, username);
            }
            else
            {
                ReflectionUtil.CopyValues(targetPrf, srcProfile);
            }

            var query = "(u:User)-[:HAS_Profile]->(prf:Profile)";

            _graphClient.Cypher
                .Match(query)
                .Where<ApplicationUser>(u => u.UserName == username)
                .Set("prf = {profile}")
                .WithParam("profile", targetPrf)
                .ExecuteWithoutResults();
            return targetPrf;
        }

        public Profile LoadProfile(string username)
        {
            var getprofileStringFormater = "(user:User {{UserName : '{0}'}})-[:HAS_Profile]->(prf:Profile)";
            var pq = _graphClient.Cypher.Match(string.Format(getprofileStringFormater, username));
            var pqr = pq.Return(prf => prf.As<Profile>());
            var profile = pqr.Results.FirstOrDefault();
            return profile;
        }
    }
}
