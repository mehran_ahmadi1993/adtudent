﻿using System;
using System.Linq;

namespace AdTudent.DAL.Util
{
    public class ReflectionUtil
    {
        public static void CopyValues<T>(T target, T source)
        {
            Type t = typeof(T);

            var properties = t.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);

            foreach (var prop in properties)
            {
                var sValue = prop.GetValue(source, null);
                var tValue = prop.GetValue(target, null);
                if (sValue != null && tValue == null)
                    prop.SetValue(target, sValue, null);
            }
        }
    }
}
