﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdTudent.Domain.POCO;
using Neo4j.AspNet.Identity;

namespace AdTudent.Domain.ViewModels
{
    public class HomeViewModel
    {
        public IList<Advertisement> Advertisements { get; set; }
        public IList<ApplicationUser> Users { get; set; }
    }
}
