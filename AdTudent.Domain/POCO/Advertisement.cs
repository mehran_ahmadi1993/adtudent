﻿using System;

namespace AdTudent.Domain.POCO
{
    public class Advertisement
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Adress { get; set; }
        public int Area { get; set; }
        public int RoomNumber { get; set; }
        public long Mortgage { get; set; }
        public int Rent { get; set; }
        public string Desc { get; set; }
        public string MobileNumber { get; set; }
        public string ImageUrl { get; set; }
        public DateTimeOffset PublishDateTime { get; set; }
    }
}
